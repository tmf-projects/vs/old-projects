﻿using System;

namespace OperatorOverload
{
	class Program
	{
		static void Main(string[] args)
		{
			Counter c1 = new Counter { Value = 22 };
			Counter c2 = new Counter { Value = 55 };

			bool result = c1 > c2;
			Counter c3 = c1 + c2;

			Console.WriteLine("Bool result: "+result);
			Console.WriteLine("Counter 3: "+c3.Value);

			result = c1 < c2;
			c3 = c3 - c2 - c1;

			Console.WriteLine("Bool result: " + result);
			Console.WriteLine("Counter 3: " + c3.Value);

			Console.ReadKey();
		}
	}
	class Counter
	{
		public int Value { get; set; }

		public static Counter operator +(Counter c1, Counter c2)
		{
			return new Counter { Value = c1.Value + c2.Value };
		}
		public static Counter operator -(Counter c1, Counter c2)
		{
			return new Counter { Value = c1.Value - c2.Value };
		}
		public static bool operator >(Counter c1, Counter c2)
		{
			if (c1.Value > c2.Value)
				return true;
			else
				return false;
		}
		public static bool operator <(Counter c1, Counter c2)
		{
			if (c1.Value < c2.Value)
				return true;
			else
				return false;
		}
	}
}
