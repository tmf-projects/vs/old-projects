﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AreaCalculation
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			labelAnswer.Text = "";
			comboBoxFigures.SelectedIndex = 0;
		}
		public static double answer;
		public static void Area(double x)
		{
			answer = Math.Pow(x, 2.0);
		}
		public static void Area(double x, double y)
		{
			answer = x*y;
		}
		public static void Area(double x, double y, double z)
		{
			double p = ((x + y + z) / 2);
			answer = Math.Sqrt(p * (p - x) * (p - y) * (p - z));
		}
		public static void AreaCircle(double r)
		{
			answer = Math.PI * Math.Pow(r, 2.0);
		}




		private void CountButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (comboBoxFigures.SelectedIndex == 0){
					Area(Convert.ToDouble(textBoxSquare.Text));
					labelAnswer.Text ="Ответ: " + Convert.ToString(answer);
				}
				if (comboBoxFigures.SelectedIndex == 1)
				{
					Area(Convert.ToDouble(textBoxRectangleLenght.Text), Convert.ToDouble(textBoxRectangleWidth.Text));
					labelAnswer.Text = "Ответ: " + Convert.ToString(answer);
				}
				if (comboBoxFigures.SelectedIndex == 2)
				{
					Area(Convert.ToDouble(textBoxTriangleSide1.Text), Convert.ToDouble(textBoxTriangleSide2.Text), Convert.ToDouble(textBoxTriangleSide3.Text));
					labelAnswer.Text = "Ответ: " + Convert.ToString(answer);
				}
				if (comboBoxFigures.SelectedIndex == 3)
				{
					AreaCircle(Convert.ToDouble(textBoxCircleRadius.Text));
					labelAnswer.Text = "Ответ: " + Convert.ToString(answer);
				}
			}
			catch (System.FormatException)
			{
				labelAnswer.Text = "Произошла ошибка!";
			}
		}

		private void comboBoxFigures_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (comboBoxFigures.SelectedIndex == 0)
			{
				labelAnswer.Text = "";
				labelSquare.Visible = true;
				textBoxSquare.Visible = true;
				labelRectangleLenght.Visible = false;
				labelRectangleWidth.Visible = false;
				textBoxRectangleLenght.Visible = false;
				textBoxRectangleWidth.Visible = false;
				labelTriangleSide1.Visible = false;
				labelTriangleSide2.Visible = false;
				labelTriangleSide3.Visible = false;
				textBoxTriangleSide1.Visible = false;
				textBoxTriangleSide2.Visible = false;
				textBoxTriangleSide3.Visible = false;
				labelCircleRadius.Visible = false;
				textBoxCircleRadius.Visible = false;
			}
			if (comboBoxFigures.SelectedIndex == 1)
			{
				labelAnswer.Text = "";
				labelSquare.Visible = false;
				textBoxSquare.Visible = false;
				labelRectangleLenght.Visible = true;
				labelRectangleWidth.Visible = true;
				textBoxRectangleLenght.Visible = true;
				textBoxRectangleWidth.Visible = true;
				labelTriangleSide1.Visible = false;
				labelTriangleSide2.Visible = false;
				labelTriangleSide3.Visible = false;
				textBoxTriangleSide1.Visible = false;
				textBoxTriangleSide2.Visible = false;
				textBoxTriangleSide3.Visible = false;
				labelCircleRadius.Visible = false;
				textBoxCircleRadius.Visible = false;
			}
			if (comboBoxFigures.SelectedIndex == 2)
			{
				labelAnswer.Text = "";
				labelSquare.Visible = false;
				textBoxSquare.Visible = false;
				labelRectangleLenght.Visible = false;
				labelRectangleWidth.Visible = false;
				textBoxRectangleLenght.Visible = false;
				textBoxRectangleWidth.Visible = false;
				labelTriangleSide1.Visible = true;
				labelTriangleSide2.Visible = true;
				labelTriangleSide3.Visible = true;
				textBoxTriangleSide1.Visible = true;
				textBoxTriangleSide2.Visible = true;
				textBoxTriangleSide3.Visible = true;
				labelCircleRadius.Visible = false;
				textBoxCircleRadius.Visible = false;
			}
			if (comboBoxFigures.SelectedIndex == 3)
			{
				labelAnswer.Text = "";
				labelSquare.Visible = false;
				textBoxSquare.Visible = false;
				labelRectangleLenght.Visible = false;
				labelRectangleWidth.Visible = false;
				textBoxRectangleLenght.Visible = false;
				textBoxRectangleWidth.Visible = false;
				labelTriangleSide1.Visible = false;
				labelTriangleSide2.Visible = false;
				labelTriangleSide3.Visible = false;
				textBoxTriangleSide1.Visible = false;
				textBoxTriangleSide2.Visible = false;
				textBoxTriangleSide3.Visible = false;
				labelCircleRadius.Visible = true;
				textBoxCircleRadius.Visible = true;
			}
		}

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
			Form2 newform = new Form2();
			newform.Show();
        }
    }
}
