﻿namespace AreaCalculation
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxSquare = new System.Windows.Forms.TextBox();
            this.labelSquare = new System.Windows.Forms.Label();
            this.comboBoxFigures = new System.Windows.Forms.ComboBox();
            this.labelFigures = new System.Windows.Forms.Label();
            this.CountButton = new System.Windows.Forms.Button();
            this.labelAnswer = new System.Windows.Forms.Label();
            this.labelRectangleLenght = new System.Windows.Forms.Label();
            this.labelRectangleWidth = new System.Windows.Forms.Label();
            this.textBoxRectangleWidth = new System.Windows.Forms.TextBox();
            this.textBoxRectangleLenght = new System.Windows.Forms.TextBox();
            this.labelTriangleSide1 = new System.Windows.Forms.Label();
            this.labelTriangleSide2 = new System.Windows.Forms.Label();
            this.labelTriangleSide3 = new System.Windows.Forms.Label();
            this.textBoxTriangleSide1 = new System.Windows.Forms.TextBox();
            this.textBoxTriangleSide2 = new System.Windows.Forms.TextBox();
            this.textBoxTriangleSide3 = new System.Windows.Forms.TextBox();
            this.labelCircleRadius = new System.Windows.Forms.Label();
            this.textBoxCircleRadius = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxSquare
            // 
            this.textBoxSquare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxSquare.Location = new System.Drawing.Point(83, 40);
            this.textBoxSquare.Name = "textBoxSquare";
            this.textBoxSquare.Size = new System.Drawing.Size(100, 26);
            this.textBoxSquare.TabIndex = 0;
            // 
            // labelSquare
            // 
            this.labelSquare.AutoSize = true;
            this.labelSquare.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSquare.Location = new System.Drawing.Point(12, 43);
            this.labelSquare.Name = "labelSquare";
            this.labelSquare.Size = new System.Drawing.Size(58, 20);
            this.labelSquare.TabIndex = 1;
            this.labelSquare.Text = "Длина";
            // 
            // comboBoxFigures
            // 
            this.comboBoxFigures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFigures.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.comboBoxFigures.FormattingEnabled = true;
            this.comboBoxFigures.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.comboBoxFigures.Items.AddRange(new object[] {
            "Квадрат",
            "Прямоугольник",
            "Треугольник",
            "Круг"});
            this.comboBoxFigures.Location = new System.Drawing.Point(288, 40);
            this.comboBoxFigures.Name = "comboBoxFigures";
            this.comboBoxFigures.Size = new System.Drawing.Size(164, 28);
            this.comboBoxFigures.TabIndex = 2;
            this.comboBoxFigures.SelectedIndexChanged += new System.EventHandler(this.comboBoxFigures_SelectedIndexChanged);
            // 
            // labelFigures
            // 
            this.labelFigures.AutoSize = true;
            this.labelFigures.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFigures.Location = new System.Drawing.Point(217, 43);
            this.labelFigures.Name = "labelFigures";
            this.labelFigures.Size = new System.Drawing.Size(65, 20);
            this.labelFigures.TabIndex = 3;
            this.labelFigures.Text = "Фигура";
            // 
            // CountButton
            // 
            this.CountButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.CountButton.Location = new System.Drawing.Point(288, 74);
            this.CountButton.Name = "CountButton";
            this.CountButton.Size = new System.Drawing.Size(164, 33);
            this.CountButton.TabIndex = 4;
            this.CountButton.Text = "Посчитать";
            this.CountButton.UseVisualStyleBackColor = true;
            this.CountButton.Click += new System.EventHandler(this.CountButton_Click);
            // 
            // labelAnswer
            // 
            this.labelAnswer.AutoSize = true;
            this.labelAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelAnswer.Location = new System.Drawing.Point(10, 160);
            this.labelAnswer.Name = "labelAnswer";
            this.labelAnswer.Size = new System.Drawing.Size(72, 20);
            this.labelAnswer.TabIndex = 5;
            this.labelAnswer.Text = "Ответ: ";
            // 
            // labelRectangleLenght
            // 
            this.labelRectangleLenght.AutoSize = true;
            this.labelRectangleLenght.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRectangleLenght.Location = new System.Drawing.Point(12, 43);
            this.labelRectangleLenght.Name = "labelRectangleLenght";
            this.labelRectangleLenght.Size = new System.Drawing.Size(58, 20);
            this.labelRectangleLenght.TabIndex = 6;
            this.labelRectangleLenght.Text = "Длина";
            // 
            // labelRectangleWidth
            // 
            this.labelRectangleWidth.AutoSize = true;
            this.labelRectangleWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRectangleWidth.Location = new System.Drawing.Point(10, 75);
            this.labelRectangleWidth.Name = "labelRectangleWidth";
            this.labelRectangleWidth.Size = new System.Drawing.Size(67, 20);
            this.labelRectangleWidth.TabIndex = 7;
            this.labelRectangleWidth.Text = "Ширина";
            // 
            // textBoxRectangleWidth
            // 
            this.textBoxRectangleWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxRectangleWidth.Location = new System.Drawing.Point(83, 72);
            this.textBoxRectangleWidth.Name = "textBoxRectangleWidth";
            this.textBoxRectangleWidth.Size = new System.Drawing.Size(100, 26);
            this.textBoxRectangleWidth.TabIndex = 8;
            // 
            // textBoxRectangleLenght
            // 
            this.textBoxRectangleLenght.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxRectangleLenght.Location = new System.Drawing.Point(83, 40);
            this.textBoxRectangleLenght.Name = "textBoxRectangleLenght";
            this.textBoxRectangleLenght.Size = new System.Drawing.Size(100, 26);
            this.textBoxRectangleLenght.TabIndex = 9;
            // 
            // labelTriangleSide1
            // 
            this.labelTriangleSide1.AutoSize = true;
            this.labelTriangleSide1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTriangleSide1.Location = new System.Drawing.Point(10, 43);
            this.labelTriangleSide1.Name = "labelTriangleSide1";
            this.labelTriangleSide1.Size = new System.Drawing.Size(87, 20);
            this.labelTriangleSide1.TabIndex = 10;
            this.labelTriangleSide1.Text = "Сторона 1";
            // 
            // labelTriangleSide2
            // 
            this.labelTriangleSide2.AutoSize = true;
            this.labelTriangleSide2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTriangleSide2.Location = new System.Drawing.Point(10, 77);
            this.labelTriangleSide2.Name = "labelTriangleSide2";
            this.labelTriangleSide2.Size = new System.Drawing.Size(87, 20);
            this.labelTriangleSide2.TabIndex = 11;
            this.labelTriangleSide2.Text = "Сторона 2";
            // 
            // labelTriangleSide3
            // 
            this.labelTriangleSide3.AutoSize = true;
            this.labelTriangleSide3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTriangleSide3.Location = new System.Drawing.Point(10, 110);
            this.labelTriangleSide3.Name = "labelTriangleSide3";
            this.labelTriangleSide3.Size = new System.Drawing.Size(87, 20);
            this.labelTriangleSide3.TabIndex = 12;
            this.labelTriangleSide3.Text = "Сторона 3";
            // 
            // textBoxTriangleSide1
            // 
            this.textBoxTriangleSide1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxTriangleSide1.Location = new System.Drawing.Point(103, 40);
            this.textBoxTriangleSide1.Name = "textBoxTriangleSide1";
            this.textBoxTriangleSide1.Size = new System.Drawing.Size(100, 26);
            this.textBoxTriangleSide1.TabIndex = 13;
            // 
            // textBoxTriangleSide2
            // 
            this.textBoxTriangleSide2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxTriangleSide2.Location = new System.Drawing.Point(103, 72);
            this.textBoxTriangleSide2.Name = "textBoxTriangleSide2";
            this.textBoxTriangleSide2.Size = new System.Drawing.Size(100, 26);
            this.textBoxTriangleSide2.TabIndex = 14;
            // 
            // textBoxTriangleSide3
            // 
            this.textBoxTriangleSide3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxTriangleSide3.Location = new System.Drawing.Point(103, 107);
            this.textBoxTriangleSide3.Name = "textBoxTriangleSide3";
            this.textBoxTriangleSide3.Size = new System.Drawing.Size(100, 26);
            this.textBoxTriangleSide3.TabIndex = 15;
            // 
            // labelCircleRadius
            // 
            this.labelCircleRadius.AutoSize = true;
            this.labelCircleRadius.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCircleRadius.Location = new System.Drawing.Point(14, 43);
            this.labelCircleRadius.Name = "labelCircleRadius";
            this.labelCircleRadius.Size = new System.Drawing.Size(63, 20);
            this.labelCircleRadius.TabIndex = 16;
            this.labelCircleRadius.Text = "Радиус";
            // 
            // textBoxCircleRadius
            // 
            this.textBoxCircleRadius.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.textBoxCircleRadius.Location = new System.Drawing.Point(83, 40);
            this.textBoxCircleRadius.Name = "textBoxCircleRadius";
            this.textBoxCircleRadius.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBoxCircleRadius.Size = new System.Drawing.Size(100, 26);
            this.textBoxCircleRadius.TabIndex = 17;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.помощьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(464, 29);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(120, 25);
            this.помощьToolStripMenuItem.Text = "О Программе";
            this.помощьToolStripMenuItem.Click += new System.EventHandler(this.помощьToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 189);
            this.Controls.Add(this.textBoxCircleRadius);
            this.Controls.Add(this.labelCircleRadius);
            this.Controls.Add(this.textBoxTriangleSide3);
            this.Controls.Add(this.textBoxTriangleSide2);
            this.Controls.Add(this.textBoxTriangleSide1);
            this.Controls.Add(this.labelTriangleSide3);
            this.Controls.Add(this.labelTriangleSide2);
            this.Controls.Add(this.labelTriangleSide1);
            this.Controls.Add(this.textBoxRectangleLenght);
            this.Controls.Add(this.textBoxRectangleWidth);
            this.Controls.Add(this.labelRectangleWidth);
            this.Controls.Add(this.labelRectangleLenght);
            this.Controls.Add(this.labelAnswer);
            this.Controls.Add(this.CountButton);
            this.Controls.Add(this.labelFigures);
            this.Controls.Add(this.comboBoxFigures);
            this.Controls.Add(this.labelSquare);
            this.Controls.Add(this.textBoxSquare);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вычисление площади";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxSquare;
        private System.Windows.Forms.Label labelSquare;
        private System.Windows.Forms.ComboBox comboBoxFigures;
        private System.Windows.Forms.Label labelFigures;
        private System.Windows.Forms.Button CountButton;
        private System.Windows.Forms.Label labelAnswer;
        private System.Windows.Forms.Label labelRectangleLenght;
        private System.Windows.Forms.Label labelRectangleWidth;
        private System.Windows.Forms.TextBox textBoxRectangleWidth;
        private System.Windows.Forms.TextBox textBoxRectangleLenght;
        private System.Windows.Forms.Label labelTriangleSide1;
        private System.Windows.Forms.Label labelTriangleSide2;
        private System.Windows.Forms.Label labelTriangleSide3;
        private System.Windows.Forms.TextBox textBoxTriangleSide1;
        private System.Windows.Forms.TextBox textBoxTriangleSide2;
        private System.Windows.Forms.TextBox textBoxTriangleSide3;
        private System.Windows.Forms.Label labelCircleRadius;
        private System.Windows.Forms.TextBox textBoxCircleRadius;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
    }
}

