﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class MyClass
    {
        int k;

        public MyClass(int i)
        {
            k = i;
        }

        public void ObjectGenerator(int i)
        {
            MyClass obj = new MyClass(i);
        }

        ~MyClass()
        {
            Console.WriteLine("Объект {0} уничтожен", k);
        }
    }
}
