﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            MyClass obj = new MyClass(0);

            for (int i = 1; i <= 12000; i++)
            {
                obj.ObjectGenerator(i);
            }
        }
    }
}
