﻿using System;

namespace IncrementDecrementOverload
{
    class Program
    {
        static void Main(string[] args)
        {
            Counter c1 = new Counter { Value = 22 };
            Counter c2 = new Counter { Value = 55 };

            Console.WriteLine("Counter 1: " + c1.Value);
            Console.WriteLine("Counter 2: " + c2.Value);

            c1++;
            Console.WriteLine("Counter 1 ++: " + c1.Value);

            ++c1;
            Console.WriteLine("++ Counter 1: " + c1.Value);

            c2--;
            Console.WriteLine("Counter 2 --: " + c2.Value);

            --c2;
            Console.WriteLine("-- Counter 2: " + c2.Value);

            Console.ReadKey();
        }
    }
    class Counter
    {
        public int Value { get; set; }

        public static Counter operator ++(Counter c1)
        {
            return new Counter { Value = c1.Value + 1 };
        }
        public static Counter operator --(Counter c1)
        {
            return new Counter { Value = c1.Value - 1 };
        }
    }
}
