﻿namespace ProjectTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label_Version = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label_Time = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label_Hours = new System.Windows.Forms.Label();
            this.label_Minutes = new System.Windows.Forms.Label();
            this.label_Seconds = new System.Windows.Forms.Label();
            this.textBox_Hours = new System.Windows.Forms.TextBox();
            this.textBox_Minutes = new System.Windows.Forms.TextBox();
            this.textBox_Seconds = new System.Windows.Forms.TextBox();
            this.label_Hours_Def = new System.Windows.Forms.Label();
            this.label_Minutes_Def = new System.Windows.Forms.Label();
            this.label_Seconds_Def = new System.Windows.Forms.Label();
            this.button_Default = new System.Windows.Forms.Button();
            this.button_InfoProgram = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_Version
            // 
            this.label_Version.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_Version.AutoSize = true;
            this.label_Version.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Version.Location = new System.Drawing.Point(12, 527);
            this.label_Version.Name = "label_Version";
            this.label_Version.Size = new System.Drawing.Size(0, 22);
            this.label_Version.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // label_Time
            // 
            this.label_Time.AutoSize = true;
            this.label_Time.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Time.Location = new System.Drawing.Point(12, 9);
            this.label_Time.Name = "label_Time";
            this.label_Time.Size = new System.Drawing.Size(0, 22);
            this.label_Time.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.button1.Location = new System.Drawing.Point(602, 478);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(170, 71);
            this.button1.TabIndex = 2;
            this.button1.Text = "Включить";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label3.Location = new System.Drawing.Point(12, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 22);
            this.label3.TabIndex = 3;
            this.label3.Text = "Будильник:";
            // 
            // label_Hours
            // 
            this.label_Hours.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Hours.AutoSize = true;
            this.label_Hours.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Hours.Location = new System.Drawing.Point(12, 162);
            this.label_Hours.Name = "label_Hours";
            this.label_Hours.Size = new System.Drawing.Size(135, 22);
            this.label_Hours.TabIndex = 4;
            this.label_Hours.Text = "Добавить часы:";
            // 
            // label_Minutes
            // 
            this.label_Minutes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Minutes.AutoSize = true;
            this.label_Minutes.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Minutes.Location = new System.Drawing.Point(12, 219);
            this.label_Minutes.Name = "label_Minutes";
            this.label_Minutes.Size = new System.Drawing.Size(156, 22);
            this.label_Minutes.TabIndex = 5;
            this.label_Minutes.Text = "Добавить минуты:";
            // 
            // label_Seconds
            // 
            this.label_Seconds.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Seconds.AutoSize = true;
            this.label_Seconds.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Seconds.Location = new System.Drawing.Point(12, 276);
            this.label_Seconds.Name = "label_Seconds";
            this.label_Seconds.Size = new System.Drawing.Size(160, 22);
            this.label_Seconds.TabIndex = 6;
            this.label_Seconds.Text = "Добавить секунды:";
            // 
            // textBox_Hours
            // 
            this.textBox_Hours.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Hours.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_Hours.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.textBox_Hours.Location = new System.Drawing.Point(16, 187);
            this.textBox_Hours.Name = "textBox_Hours";
            this.textBox_Hours.Size = new System.Drawing.Size(131, 29);
            this.textBox_Hours.TabIndex = 7;
            this.textBox_Hours.Text = "5";
            this.textBox_Hours.Click += new System.EventHandler(this.textBox_Hours_Click);
            this.textBox_Hours.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Hours_KeyPress);
            this.textBox_Hours.Leave += new System.EventHandler(this.textBox_Hours_Leave);
            // 
            // textBox_Minutes
            // 
            this.textBox_Minutes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Minutes.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_Minutes.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.textBox_Minutes.Location = new System.Drawing.Point(16, 244);
            this.textBox_Minutes.Name = "textBox_Minutes";
            this.textBox_Minutes.Size = new System.Drawing.Size(152, 29);
            this.textBox_Minutes.TabIndex = 8;
            this.textBox_Minutes.Text = "30";
            this.textBox_Minutes.Click += new System.EventHandler(this.textBox_Minutes_Click);
            this.textBox_Minutes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Minutes_KeyPress);
            this.textBox_Minutes.Leave += new System.EventHandler(this.textBox_Minutes_Leave);
            // 
            // textBox_Seconds
            // 
            this.textBox_Seconds.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Seconds.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox_Seconds.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.textBox_Seconds.Location = new System.Drawing.Point(16, 301);
            this.textBox_Seconds.Name = "textBox_Seconds";
            this.textBox_Seconds.Size = new System.Drawing.Size(152, 29);
            this.textBox_Seconds.TabIndex = 9;
            this.textBox_Seconds.Text = "25";
            this.textBox_Seconds.Click += new System.EventHandler(this.textBox_Seconds_Click);
            this.textBox_Seconds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_Seconds_KeyPress);
            this.textBox_Seconds.Leave += new System.EventHandler(this.textBox_Seconds_Leave);
            // 
            // label_Hours_Def
            // 
            this.label_Hours_Def.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Hours_Def.AutoSize = true;
            this.label_Hours_Def.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Hours_Def.Location = new System.Drawing.Point(153, 190);
            this.label_Hours_Def.Name = "label_Hours_Def";
            this.label_Hours_Def.Size = new System.Drawing.Size(139, 22);
            this.label_Hours_Def.TabIndex = 10;
            this.label_Hours_Def.Text = "(По умолчанию)";
            // 
            // label_Minutes_Def
            // 
            this.label_Minutes_Def.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Minutes_Def.AutoSize = true;
            this.label_Minutes_Def.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Minutes_Def.Location = new System.Drawing.Point(174, 247);
            this.label_Minutes_Def.Name = "label_Minutes_Def";
            this.label_Minutes_Def.Size = new System.Drawing.Size(139, 22);
            this.label_Minutes_Def.TabIndex = 11;
            this.label_Minutes_Def.Text = "(По умолчанию)";
            // 
            // label_Seconds_Def
            // 
            this.label_Seconds_Def.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_Seconds_Def.AutoSize = true;
            this.label_Seconds_Def.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.label_Seconds_Def.Location = new System.Drawing.Point(174, 304);
            this.label_Seconds_Def.Name = "label_Seconds_Def";
            this.label_Seconds_Def.Size = new System.Drawing.Size(139, 22);
            this.label_Seconds_Def.TabIndex = 12;
            this.label_Seconds_Def.Text = "(По умолчанию)";
            // 
            // button_Default
            // 
            this.button_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Default.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button_Default.FlatAppearance.BorderSize = 0;
            this.button_Default.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Default.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.button_Default.Location = new System.Drawing.Point(602, 401);
            this.button_Default.Name = "button_Default";
            this.button_Default.Size = new System.Drawing.Size(170, 71);
            this.button_Default.TabIndex = 13;
            this.button_Default.Text = "Значения по умолчанию";
            this.button_Default.UseVisualStyleBackColor = false;
            this.button_Default.Click += new System.EventHandler(this.button_Default_Click);
            // 
            // button_InfoProgram
            // 
            this.button_InfoProgram.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_InfoProgram.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button_InfoProgram.FlatAppearance.BorderSize = 0;
            this.button_InfoProgram.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_InfoProgram.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.button_InfoProgram.Location = new System.Drawing.Point(602, 12);
            this.button_InfoProgram.Name = "button_InfoProgram";
            this.button_InfoProgram.Size = new System.Drawing.Size(170, 71);
            this.button_InfoProgram.TabIndex = 14;
            this.button_InfoProgram.Text = "О Программе";
            this.button_InfoProgram.UseVisualStyleBackColor = false;
            this.button_InfoProgram.Click += new System.EventHandler(this.button_InfoProgram_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.button_InfoProgram);
            this.Controls.Add(this.button_Default);
            this.Controls.Add(this.label_Seconds_Def);
            this.Controls.Add(this.label_Minutes_Def);
            this.Controls.Add(this.label_Hours_Def);
            this.Controls.Add(this.textBox_Seconds);
            this.Controls.Add(this.textBox_Minutes);
            this.Controls.Add(this.textBox_Hours);
            this.Controls.Add(this.label_Seconds);
            this.Controls.Add(this.label_Minutes);
            this.Controls.Add(this.label_Hours);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label_Time);
            this.Controls.Add(this.label_Version);
            this.MinimumSize = new System.Drawing.Size(400, 500);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Будильник";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Version;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label_Time;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_Hours;
        private System.Windows.Forms.Label label_Minutes;
        private System.Windows.Forms.Label label_Seconds;
        private System.Windows.Forms.TextBox textBox_Hours;
        private System.Windows.Forms.TextBox textBox_Minutes;
        private System.Windows.Forms.TextBox textBox_Seconds;
        private System.Windows.Forms.Label label_Hours_Def;
        private System.Windows.Forms.Label label_Minutes_Def;
        private System.Windows.Forms.Label label_Seconds_Def;
        private System.Windows.Forms.Button button_Default;
        private System.Windows.Forms.Button button_InfoProgram;
    }
}

