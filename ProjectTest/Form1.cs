﻿using System;
using System.Windows.Forms;

namespace ProjectTest
{
    public partial class Form1 : Form
    {
        DateTime nowTime = DateTime.Now;
        DateTime secondTime = DateTime.Now;
        bool timer = false;
        public Form1()
        {
            InitializeComponent();
            label_Version.Text = nowTime.ToString();
            label_Time.Text = GetTime(nowTime, secondTime);
            timer1.Start();
        }

        void Timer1_Tick(object sender, EventArgs e)
        {
            nowTime = DateTime.Now;
            label_Version.Text = $"Версия: {ProductVersion}";
            label_Time.Text = GetTime(nowTime, secondTime);

            if (timer)
            {
                CheckTime();
            }
        }

        string GetTime(DateTime nowTime, DateTime secondTime)
        {
            if (timer)
            {
                return $@"Текущее время: {nowTime}
Будильник: {secondTime}
Таймер: {(secondTime.Hour - nowTime.Hour).ToString("00")}:{(secondTime.Minute - nowTime.Minute).ToString("00")}:{(secondTime.Second - nowTime.Second).ToString("00")}";
            }
            else
            {
                return $@"Текущее время: {nowTime}";
            }
        }

        void CheckTime()
        {
            if (nowTime.Hour == secondTime.Hour &
                nowTime.Minute == secondTime.Minute &
                nowTime.Second == secondTime.Second)
            {
                timer = false;
                button1.Text = "Включить";
                MsgBox(0);
            }
            else if (secondTime.Hour - nowTime.Hour <= -1 ||
                secondTime.Minute - nowTime.Minute <= -1 ||
                secondTime.Second - nowTime.Second <= -1)
            {
                timer = false;
                button1.Text = "Включить";
                MsgBox(4);
            }
        }

        void MsgBox(byte IaE)
        {
            switch (IaE)
            {
                case 0:
                    MessageBox.Show("Время вышло!",
                    "Будильник", MessageBoxButtons.OK, MessageBoxIcon.Information); break;
                case 1:
                    MessageBox.Show(@"Произошла ошибка!
Возможно, вы случайно ввели не те символы в поля ввода. Нужны числовые символы.",
                    "Будильник", MessageBoxButtons.OK, MessageBoxIcon.Error); break;
                case 2:
                    MessageBox.Show($@"Предупреждение! В полях ввода нельзя писать буквы,
спец. символы, оставлять поле пустым.
В случае, если вы хотите добавить только секунды, в поле для добавления секунд нельзя писать цифру 0.",
                            "Будильник", MessageBoxButtons.OK, MessageBoxIcon.Warning); break;
                case 3:
                    MessageBox.Show($@"{ProductName}, версия {ProductVersion}",
                "О Программе", MessageBoxButtons.OK, MessageBoxIcon.Information); break;
                case 4:
                    MessageBox.Show(@"Произошла ошибка!
Отрицательные значения в таймере!",
                    "Будильник", MessageBoxButtons.OK, MessageBoxIcon.Error); break;
            }
        }

        void StartAlarmClock()
        {
            secondTime = DateTime.Now;
            try
            {
                if (textBox_Hours.Text != "" &
                    textBox_Minutes.Text != "" &
                    textBox_Seconds.Text != "" &
                    textBox_Seconds.Text != "0")
                {
                    secondTime = secondTime.AddHours(Convert.ToDouble(textBox_Hours.Text));
                    secondTime = secondTime.AddMinutes(Convert.ToDouble(textBox_Minutes.Text));
                    secondTime = secondTime.AddSeconds(Convert.ToDouble(textBox_Seconds.Text));

                    timer = true;
                }
                else if (textBox_Minutes.Text != "" &
                    textBox_Seconds.Text != "" &
                    textBox_Seconds.Text != "0")
                {
                    secondTime = secondTime.AddMinutes(Convert.ToDouble(textBox_Minutes.Text));
                    secondTime = secondTime.AddSeconds(Convert.ToDouble(textBox_Seconds.Text));

                    timer = true;
                }
                else if (textBox_Seconds.Text != "" &
                    textBox_Seconds.Text != "0")
                {
                    secondTime = secondTime.AddSeconds(Convert.ToDouble(textBox_Seconds.Text));

                    timer = true;
                }
                else
                {
                    button1.Text = "Включить";
                    MsgBox(2);
                }
            }
            catch (FormatException)
            {
                button1.Text = "Включить";
                MsgBox(1);
            }
        }

        void Button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Включить")
            {
                button1.Text = "Выключить";
                StartAlarmClock();
            }
            else
            {
                button1.Text = "Включить";
                timer = false;
            }
        }

        void textBox_Hours_Click(object sender, EventArgs e)
        {
            textBox_Hours.Text = "";
            label_Hours_Def.Visible = false;
        }

        void textBox_Minutes_Click(object sender, EventArgs e)
        {
            textBox_Minutes.Text = "";
            label_Minutes_Def.Visible = false;
        }

        void textBox_Seconds_Click(object sender, EventArgs e)
        {
            textBox_Seconds.Text = "";
            label_Seconds_Def.Visible = false;
        }

        void button_Default_Click(object sender, EventArgs e)
        {
            textBox_Hours.Text = "5";
            textBox_Minutes.Text = "30";
            textBox_Seconds.Text = "25";

            label_Hours_Def.Visible = true;
            label_Minutes_Def.Visible = true;
            label_Seconds_Def.Visible = true;
        }

        void Form1_SizeChanged(object sender, EventArgs e)
        {
            //Text = Size.ToString();
            if (Size.Width < 510)
            {
                button_InfoProgram.Visible = false;
            }
            else
            {
                button_InfoProgram.Visible = true;
            }
        }

        void button_InfoProgram_Click(object sender, EventArgs e)
        {
            MsgBox(3);
        }

        private void textBox_Hours_Leave(object sender, EventArgs e)
        {
            if (textBox_Hours.Text == "")
            {
                textBox_Hours.Text = "0";
            }
        }

        private void textBox_Minutes_Leave(object sender, EventArgs e)
        {
            if (textBox_Minutes.Text == "")
            {
                textBox_Minutes.Text = "0";
            }
        }

        private void textBox_Seconds_Leave(object sender, EventArgs e)
        {
            if (textBox_Seconds.Text == "" || textBox_Seconds.Text == "0")
            {
                textBox_Seconds.Text = "1";
            }
        }

        void CheckPressed(KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar) || Char.IsWhiteSpace(e.KeyChar) || Char.IsPunctuation(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void textBox_Hours_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckPressed(e);
        }

        private void textBox_Minutes_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckPressed(e);
        }

        private void textBox_Seconds_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckPressed(e);
        }
    }
}
