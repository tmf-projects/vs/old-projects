﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR_Event
{
    class TestClass
    {
        public event EventHandler onChanged;

        private string title;

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                onChanged(this, new EventArgs());
            }
        }
    }
}
