﻿using System;
using System.Windows.Forms;

namespace PR_Event
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestClass tc = new TestClass();
            tc.onChanged += ShowMessageBox;
            tc.Title = "Clicked";
        }

        private void ShowMessageBox(object sender, EventArgs e)
        {
            MessageBox.Show("Событие 1 выполнено!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TestClass tc = new TestClass();
            tc.onChanged += ShowMessageBox2;
            tc.Title = "Clicked";
        }

        private void ShowMessageBox2(object sender, EventArgs e)
        {
            MessageBox.Show("Событие 2 выполнено!");
        }
    }
}
