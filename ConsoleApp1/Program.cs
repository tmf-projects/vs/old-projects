﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public static string start;
        public static double x;
        public static bool working = true;
        public static string a;

        public static void Area(double x)
        {
            double answer = Math.Pow(x, 2.0);
            Console.WriteLine(answer);
            Console.WriteLine("");
        }
        static void Main(string[] args)
        {
            /*string[] yes = new string[10];

            yes[0] = "Y";
            yes[1] = "y";
            yes[2] = "Д";
            yes[3] = "д";
            yes[4] = "Yes";
            yes[5] = "yes";
            yes[6] = "Да";
            yes[7] = "да";*/

            string[] no = new string[8];

            no[0] = "N";
            no[1] = "n";
            no[2] = "Н";
            no[3] = "н";
            no[4] = "No";
            no[5] = "no";
            no[6] = "Нет";
            no[7] = "нет";

            while (working) {
                Console.WriteLine("Введите число");
                start = Console.ReadLine();
                Console.WriteLine("");

                try
                {
                    x = Convert.ToDouble(start);
                    Area(x);
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Произошла ошибка! Возможно, вы ввели букву.");
                    Console.WriteLine("");
                }
                finally
                {
                    Console.WriteLine("Начнём заново?");
                    a = Console.ReadLine();
                    int i;
                    for (i = 0; i <= 7; i++)
                    {
                        if (a == no[i])
                        {
                            working = false;
                        }
                    }
                    Console.WriteLine("");
                }
            }
        }
    }
}
